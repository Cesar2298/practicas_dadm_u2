package com.example.prcticas_unidad2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class Participant_17550794: RecyclerView.Adapter<Participant_17550794ViewHolder>(){
// Creamos la Clase para nuestro adaptador, y como vamos a usar un RecyclerView ajustamos los parametros para ello
    //Colocamos dentro del adaptador el que creamos en la clase que se encuentra abajo
    //Posteriormente debemos completar los parametros que nos faltan para lo que usamos Click Derecho y damos en implementar
    val list = mutableListOf<Pa>()
// Creamos una lista en este caso un lista mutable y le asignamos el nombre de nuestra data class
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Participant_17550794ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_list,parent, false)
// Creamos la variable ItemView con el LayoutInflater, el cual requiere un contexto, luego lo inflamos con la ItemList que creamos
// pasamos el parente y le decimos si tiene que estar tachado a la raiz o no
        return Participant_17550794ViewHolder(itemView)
    //El CreateViewHolder nos pide que le devolvamos un ViewHolder

    }

    override fun onBindViewHolder(holder: Participant_17550794ViewHolder, position: Int) {
        holder.setData(list[position])
        //Obtenemos el Holder y la posicion del Item, lo que necesitamos en el setData también lo usamos aquí
    }

    override fun getItemCount(): Int {
        return list.size
        //El ItemCount nos pide que le devolvamos un Int por eso ponemos la lista donde viene cada Int que vamos a usar
    }

}

class Participant_17550794ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
// Creamos esta clase para que sea el adaptador de la clase principal y como vamos a usar un RecyclerView lo incluimos
    fun setData(pa: Pa){

    }
}

data class  Pa(var name: String, var apellido: String, var edad: String, var profesion: String)
//Esta Data Class funciona para rellenar y los datos que deseamos colocar dentro de la lista, tambien definimos que va a solicitar la lista

data class  Music(var Nombre: String, var Álbum: String, var Artista: String, var Genero: String, var Año_de_lanzamiento: Int, var Duracion: Int)