package com.example.prcticas_unidad2.practica1

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import androidx.core.app.ActivityCompat
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.activity_practica1.*
import java.util.zip.CheckedInputStream

class Practica1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica1)

        btnCal.setOnClickListener {
            //Obtener el telefono
            val telephone = etPhone.text.toString()
            if (BS.isChecked){ //ES MENSAJE
                //Obtener el mensaje
                if (validateMsgPermissions()){
                    var sms : SmsManager = SmsManager.getDefault()
                    sms.sendTextMessage(etPhone.text.toString(),"ME", txtM.text.toString(),null, null)

                } else {
                    val permission = arrayOf(Manifest.permission.SEND_SMS)
                    ActivityCompat.requestPermissions(this, permission, 102)
                }
            } else { //ES LLAMADA
                if (validateCallPermissions()) {//Ya tenemos permiso
                    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: $telephone"))
                    startActivity(intent)
                } else {//No tenemos Permiso
                    val permission = arrayOf(Manifest.permission.CALL_PHONE)
                    ActivityCompat.requestPermissions(this, permission, 101)
                }
            }
        }
    }
    private fun validateCallPermissions(): Boolean {

        return ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED
    }

    private fun validateMsgPermissions(): Boolean {

        return ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
    }
}
