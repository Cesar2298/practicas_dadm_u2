package com.example.prcticas_unidad2.practica6

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.item_city_timezone.view.*
import kotlinx.android.synthetic.main.item_list.view.*

class CitiesTimeZoneAdapter(private val listener: (CitiesAndTimeZone.CitiesTimezones) -> Unit): RecyclerView.Adapter<CitiesTimeZoneViewHolder>() {

    private var list = mutableListOf<CitiesAndTimeZone.CitiesTimezones>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CitiesTimeZoneViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_city_timezone,parent, false)
        return CitiesTimeZoneViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CitiesTimeZoneViewHolder, position: Int) {
        holder.setData(list[position], listener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: List<CitiesAndTimeZone.CitiesTimezones>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }

}

class CitiesTimeZoneViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

    fun setData(item: CitiesAndTimeZone.CitiesTimezones, listener: (CitiesAndTimeZone.CitiesTimezones) -> Unit){
        itemView.apply {

            tvCC.text = "${item.name}/${item.country}"
            tvTZ.text = item.timeZoneName
            setOnClickListener { listener.invoke(item)
            }
        }
    }

}