package com.example.prcticas_unidad2.practica_integradora

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.prcticas_unidad2.R
import com.example.prcticas_unidad2.practica6.CitiesAndTimeZone
import kotlinx.android.synthetic.main.item_city_timezone.view.*
import kotlinx.android.synthetic.main.item_weather.view.*
import java.text.SimpleDateFormat
import java.util.*

class AdapterExample(private val listener: (String, Weather) -> Unit): RecyclerView.Adapter<AdapterExampleViewHolder>() {


    val listWeather = mutableListOf<Weather>()
    val listNames = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterExampleViewHolder {
        val itemView =
                LayoutInflater.from(parent.context).inflate(R.layout.item_weather,parent,false)

        return AdapterExampleViewHolder(itemView)
        //El CreateViewHolder nos pide que le devolvamos un ViewHolder
    }

    override fun onBindViewHolder(holder: AdapterExampleViewHolder, position: Int) {
        holder.setData(listNames[position], listWeather[position])
    }

    override fun getItemCount(): Int {
        return listWeather.size
    }
    fun setList(listNames: List<String>, listWeather: List<Weather>){
        this.listNames.addAll(listNames)
    }
}

    class AdapterExampleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        fun setData(cityName: String, weather: Weather, listener: (listNames: List<String>, listWeather: List<Weather>) -> Unit){
            itemView.apply {


                txtCity.text= item.timezone
                txtPro.text = weather.daily.
                txtTemp.text = item.summary
                ttDay.text = item.time
                ImgT1.setImageIcon() = item.Icon
                ImgT2.setImageIcon() = item.Icon


        }
        fun getDayOfTheWeek(date: Date): String{
            val format = SimpleDateFormat("EEEE,d",Locale.getDefault())
            return format.format(date)
        }
        fun getWeatherIcon(name: String): Int{
            return when (name) {
                "clear-day" -> R.drawable.wic_clear_day
                "clear-night" ->R.drawable.wic_clear_night
                "rain" ->R.drawable.wic_rain
                "snow" ->R.drawable.wic_snow
                "wind" ->R.drawable.wic_wind
                "fog" ->R.drawable.wic_fog
                "sleet" ->R.drawable.wic_sleet
                "cloudy" ->R.drawable.wic_cloudy
                "party-cloudy-day" ->R.drawable.wic_partly_cloudy_day
                "party-cloudy-night" ->R.drawable.wic_partly_cloudy_night
                "hail" ->R.drawable.wic_hail
                "thunderstorm" ->R.drawable.wic_thunderstorm
                "tornado" ->R.drawable.wic_tornado

            else ->{
                R.drawable.ic_launcher_background
            }
        }
    }
data class  DADM(var name: String, var game: String)

fun setList(arrayCities: List<String>, weatherList: MutableList<Weather>) {
    TODO("Not yet implemented")
              }
        }
        }