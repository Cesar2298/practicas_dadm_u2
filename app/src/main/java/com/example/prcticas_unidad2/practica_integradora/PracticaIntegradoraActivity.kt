package com.example.prcticas_unidad2.practica_integradora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.activity_practica5.*
import kotlinx.android.synthetic.main.activity_practica_integradora.*
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter


class PracticaIntegradoraActivity : AppCompatActivity() {

    private var arrayCities = listOf<String>("Seoul","Madrid","Chihuahua")

    private val adapter by lazy {
        AdapterExample{ cityName, weather ->
            val imtent = Intent()
            intent.putExtra("CITY", cityName)

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica_integradora)
        lista.adapter = adapter

//        intent.extras?.getString("CITY")


        val inputS = resources.openRawResource(R.raw.weather)
        val writer = StringWriter()
        val buffer = CharArray(1024)
        inputS.use { input ->
            val reader = BufferedReader(InputStreamReader(input, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also {n = it} != -1){
                writer.write(buffer, 0,n)
            }
        }

        val weatherList = mutableListOf<Weather>()
        val jsonFile = JSONObject(writer.toString())
        arrayCities.forEach { city ->
            val cityString = jsonFile.getString(city)
            val jsonCity = JSONObject(cityString)
            val actualweather = Weather(jsonCity)
            weatherList.add(actualweather)
        }

        weatherList.forEach {
            Log.e("WEATHER", "${it.timezone}, ${it.daily}")

        }
        adapter.setList(arrayCities, weatherList)


    }
}