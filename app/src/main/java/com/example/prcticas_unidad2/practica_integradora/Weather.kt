package com.example.prcticas_unidad2.practica_integradora

import org.json.JSONObject
import kotlin.collections.ArrayList

class Weather (jsonObject: JSONObject){


    var timezone: String = ""
    var daily: Daily

    init{
        val dailyJo= jsonObject.getJSONObject("daily")
        daily = Daily(dailyJo)

    }

    class Daily(dailyJo: JSONObject){
        var data: ArrayList<Weather> = arrayListOf()
        var icon: String= ""
        var summary: String= ""

        init {
            icon = dailyJo.getString("icon")
            summary = dailyJo.getString("summary")
            val dataJa = dailyJo.getJSONArray("data")
            for ( i in 0 until dataJa.length()){
                val dataJo = dataJa.getJSONObject(i)
                data.add(Weather(dataJo))
            }
        }
    }

}