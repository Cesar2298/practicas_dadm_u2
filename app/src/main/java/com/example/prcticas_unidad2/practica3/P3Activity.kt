package com.example.prcticas_unidad2.practica3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.activity_p3.*

class P3Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_p3)

        var bundle = Bundle()
        bundle = intent.extras ?: Bundle()
        val name = bundle.getString("NAME")
        val lastName= bundle.getString("LastNAME")
        val age = bundle.getInt("AGE")
        val salary = bundle.getInt("SALARY")
        val gender = bundle.getString("GENDER")
        tvInfo.text = String.format(getString(R.string.info), name, lastName, age, salary, gender)





    }
}