package com.example.prcticas_unidad2.practica3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.activity_practica3.*

class Practica3Activity : AppCompatActivity() {

    var gender = com.example.prcticas_unidad2.practica3.gender.MALE.namee

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica3)


        btnShow.setOnClickListener {
           //Validar Datos

            val bundle =Bundle()
            val intent = Intent (this, P3Activity::class.java)

            if (etName.text.isNotEmpty() && etLastName.text.isNotEmpty() && etAge.text.isNotEmpty() && etSalary.text.isNotEmpty()) {
                bundle.putString("NAME", etName.text.toString())
                bundle.putString("LASTNAME", etLastName.text.toString())
                bundle.putInt("AGE", etAge.text.toString().toInt())
                bundle.putInt("SALARY", etSalary.text.toString().toInt())
                bundle.putString("GENDER", gender)


                intent.putExtras(bundle)
                startActivity(intent)
            }
            else{
                Toast.makeText(this, "",Toast.LENGTH_SHORT).show()
            }
        }

        rgGender.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId){
                R.id.rbMale->{
                   gender = com.example.prcticas_unidad2.practica3.gender.MALE.namee

                }
                R.id.rbFemale->{
                    gender = com.example.prcticas_unidad2.practica3.gender.FEMALE.namee
                }
                R.id.rbNB->{
                    gender = com.example.prcticas_unidad2.practica3.gender.NOT_BINARY.namee
                }
                R.id.rbNI->{
                    gender = com.example.prcticas_unidad2.practica3.gender.NOT_IDENTYFIED.namee
                }

            }
        }




    }
}