package com.example.prcticas_unidad2.practica2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.activity_duration_selector.*

class DurationSelectorActivity : AppCompatActivity() {


    var durationlist = mutableListOf<Int>()

    private val adapter by lazy {
        DurationAdapter { duration ->
            intent.putExtra("DURATION", duration)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_duration_selector)
        durationlist = mutableListOf(5, 10, 15, 20, 25, 30, 60, 120, 180, 240)
        rvDuration.adapter = adapter
        adapter.setList(durationlist)

    }
}