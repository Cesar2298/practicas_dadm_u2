package com.example.prcticas_unidad2.practica5

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.activity_practica5.*

class Practica5Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica5)


        btnM.setOnClickListener {
            val intent = Intent (this, ListActivity:: class.java)
            startActivity(intent)
        }

    }
}