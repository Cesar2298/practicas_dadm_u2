package com.example.prcticas_unidad2.practica5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.item_menu.view.*


class CustomAdapter(private val listener: (Menu, Int) -> Unit): RecyclerView.Adapter<CustomAdapterViewHolder>() {

    /*private var list: List<Person> = emptyList()*/
    private var list: MutableList<Menu> = mutableListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomAdapterViewHolder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false)
        return CustomAdapterViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: CustomAdapterViewHolder, position: Int) {
        holder.setData(list[position], position, listener)
    }
    override fun getItemCount(): Int {
        return list.size
    }
    fun setList(list: List<Menu>){
        /*this.list = list*/
        this.list.addAll(list)
    }
    fun addPerson(menu: Menu){
        this.list.add(0, menu)
        //notifyDataSetChanged()   Esto vuelve a cargar toda la lista
        notifyItemInserted(0)
    }                     /*list.size-1   ponerlo al final de la lista*/

}
class CustomAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    fun setData(menu: Menu, position: Int, listener: (Menu, Int) ->Unit ){
        itemView.apply {
            txtT.text = menu.toString()
            txtP.text = menu.toString()
            setOnClickListener{
                listener.invoke(menu, position)
            }
        }
    }
}
data class Menu(var txtT:String,var txtP:String)

