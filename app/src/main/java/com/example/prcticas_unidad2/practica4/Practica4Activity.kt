package com.example.prcticas_unidad2.practica4

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.prcticas_unidad2.R
import kotlinx.android.synthetic.main.activity_practica4.*

class Practica4Activity : AppCompatActivity() {
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica4)
        

        btnAd.setOnClickListener {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setMessage("Esto es un cuadro de dialogo")
                    .setTitle("Hola")
                    //.setCancelable(false) Para evitar que se salga del cuadro de dialogo

                    //.setPositiveButton("SIMON"){dialog,which ->}

                    .setPositiveButton("SIMON", DialogInterface.OnClickListener { dialog0, which ->
                        Toast.makeText(this,"Amonos", Toast.LENGTH_SHORT).show()
                    })
                    .setNeutralButton("No lo sé", DialogInterface.OnClickListener { dialog0, which ->
                        Toast.makeText(this,"Amonos", Toast.LENGTH_SHORT).show()
                    })
                    .setNegativeButton("NELSON", DialogInterface.OnClickListener { dialog, which ->
                        Toast.makeText(this, "Ihh", Toast.LENGTH_SHORT).show()
                    })
 
            val dialog: AlertDialog = builder.create()
            dialog.show()

        }
        btnAd2.setOnClickListener {
            val colors = arrayOf("Red","Blue","Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                    .setTitle("Escoje un color")
                    .setItems(colors){dialog,whitch ->
                        //which contiene el index del item seleccionado
                        when(whitch){
                            0 -> Toast.makeText(this, "Red", Toast.LENGTH_SHORT).show()
                            1 -> Toast.makeText(this, "Blue", Toast.LENGTH_SHORT).show()
                            2 -> Toast.makeText(this, "Green", Toast.LENGTH_SHORT).show()
                            3 -> Toast.makeText(this, "Yellow", Toast.LENGTH_SHORT).show()
                        }
                    }
            val dialog = builder.create()
            dialog.show()
        }

        btnAd3.setOnClickListener {
            val colors = arrayOf("Red","Blue","Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                    .setTitle("Escoje un color")
                    .setSingleChoiceItems(colors, 0) { dialog, which ->
                    Toast.makeText(this, "Seleccionaste: ${colors[which]}", Toast.LENGTH_SHORT).show()
                    }
            val dialog = builder.create()
            dialog.show()
        }
        val selectedItems = arrayListOf<Int>()
        btnAd4.setOnClickListener {
            val colors = arrayOf("Red","Blue","Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                    .setTitle("Escoje un color")
                    .setMultiChoiceItems( colors, null){ dialog, which, isChecked ->
                        if (isChecked){
                            //guardar el indice
                            selectedItems.add(which)
                            Toast.makeText(this, "Selected Items ${selectedItems.size}", Toast.LENGTH_SHORT).show()
                        }
                        else if (selectedItems.contains(which)){
                            //remover el indice
                            selectedItems.remove(which)
                            Toast.makeText(this, "Selected Items ${selectedItems.size}", Toast.LENGTH_SHORT).show()

                        }
                    }
            val dialog = builder.create()
            dialog.show()
        }
        btnAd5.setOnClickListener {

            Common.customDialog(this){yes ->

                if(yes){
                    Toast.makeText(this, "Yes", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "No", Toast.LENGTH_SHORT).show()
                }

            }.show()

        }
        
    }

}