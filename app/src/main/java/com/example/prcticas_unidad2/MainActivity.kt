package com.example.prcticas_unidad2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.prcticas_unidad2.practica1.Practica1Activity
import com.example.prcticas_unidad2.practica2.Practica2Activity
import com.example.prcticas_unidad2.practica3.Practica3Activity
import com.example.prcticas_unidad2.practica4.Practica4Activity
import com.example.prcticas_unidad2.practica5.Practica5Activity
import com.example.prcticas_unidad2.practica6.Practica6Activity
import com.example.prcticas_unidad2.practica_integradora.PracticaIntegradoraActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnPractica1.setOnClickListener {
             val intent = Intent (this, Practica1Activity:: class.java)
            startActivity(intent)
        }

        btnPractica2.setOnClickListener {
            val intent = Intent (this, Practica2Activity:: class.java)
            startActivity(intent)
        }
        btnPractica3.setOnClickListener {
            val intent = Intent (this, Practica3Activity:: class.java)
            startActivity(intent)
        }
        btnPractica4.setOnClickListener {
            val intent = Intent (this, Practica4Activity:: class.java)
            startActivity(intent)
        }
        btnPractica5.setOnClickListener {
            val intent = Intent (this, Practica5Activity:: class.java)
            startActivity(intent)
        }
        btnPractica6.setOnClickListener {
            val intent = Intent (this, Practica6Activity:: class.java)
            startActivity(intent)
        }
        btnPracticaI.setOnClickListener {
            val intent = Intent (this, PracticaIntegradoraActivity:: class.java)
            startActivity(intent)
        }
    }
}